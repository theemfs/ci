#!/usr/bin/env bash

if [[ "$OSTYPE" == "darwin"* ]]; then SED_ARG="-E"; else SED_ARG="-r"; fi

declare -gx IMAGE_TAG=$(echo "${CI_COMMIT_REF_NAME}" | sed "$SED_ARG" 's!^.*/!!g')

docker_image_create(){
    check_gitlab_vars
    COMPONENT_PATH=$1
    TAG=$2
    OPTIONS=""
    # set -x
    DOCKER_PATH=$([[ -n $DOCKERFILE_PATH ]] && echo "-f $DOCKERFILE_PATH ." || echo "." )

    LABELS=$(echo --label branch="$CI_COMMIT_REF_NAME" \
                  --label commit="$CI_COMMIT_SHA" \
                  --label build="$CI_JOB_ID" \
                  --label project="$CI_PROJECT_PATH")

    ARGS=$(echo   --build-arg GIT_BRANCH="$CI_COMMIT_REF_NAME" \
                  --build-arg GIT_COMMIT_SHA="$CI_COMMIT_SHA" \
                  --build-arg CI_JOB_TOKEN \
                  --build-arg SERVICE_NAME \
                  "$CUSTOM_DOCKER_ARGS" \
                  --build-arg BUILD_TIME="$(date -u +"%Y-%m-%dT%H:%M:%S%z")")

    IMAGE_NAME="$COMPONENT_PATH:$TAG"
    DOCKER_IMAGE=$CI_REGISTRY_IMAGE$IMAGE_NAME

    if [[ ${K8S_ENVIRONMENT} == "infra-ts" ]]; then
    OPTIONS=$(echo --network=host)
    fi

    echo "Build Docker image $DOCKER_IMAGE"
    docker build $OPTIONS $LABELS -t "$DOCKER_IMAGE" $ARGS $DOCKER_PATH

    echo "Push Docker image $DOCKER_IMAGE"
    docker push "$DOCKER_IMAGE"
}


check_gitlab_vars(){
    if [[ -z $CI_COMMIT_REF_NAME || -z $CI_COMMIT_SHA || -z $CI_PIPELINE_ID ]]; then
        print_error 'gitlabs variables are undefined' 1
    fi
}